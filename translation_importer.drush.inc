<?php
/**
 * @file
 * Drush integration functions for the translation importer.
 */

/**
 * Implements hook_drush_command().
 */
function translation_importer_drush_command() {
  $items['translation-importer-update'] = array(
    'description' => 'Import custom translations.',
    'callback' => 'drush_translation_importer_update',
    'drupal dependencies' => array('translation_importer'),
    'aliases' => array('timp', 'translation_importer'),
  );
  return $items;
}

/**
 * Drush callback; update the admin role with all current available permissions.
 */
function drush_translation_importer_update() {
  drush_print(dt('Translations import update started.'));
  translation_importer_import_catalogs(TRUE);
}
